# Cloud Storage Demos



## Signed Url 
Este script en Python genera una URL firmada para acceder a un objeto específico dentro de un bucket de Google Cloud Storage (GCS) durante un tiempo limitado. 

Consideraciones importantes:

En los parámetros de la función **generate_signed_url()**. **credentials_file_path** es un valor (Opcional) y representa la ruta al archivo JSON de credenciales de la cuenta de servicio (por defecto: "misw-soluciones-cloud.json"). En este caso yo subí el archivo JSON a la misma carpeta del proyecto, por lo cual no le paso el parámetro a la función en el bloque main, para esto el nombre del archivo debe ser idéntico al parámetro. 

En el bloque principal se ejecuta solo cuando el script se ejecuta directamente, no cuando se importa como un módulo. Este bloque define el nombre del bucket como **cloud-misw-2** y el nombre del objeto a firmar como **upload/foto.png**. Estos valores corresponden a mi caso específico y son solo de ejemplo, el nombre global de su bucket será diferente, por lo que debe actualizar este valor, y probablemente igual con sus directorios y archivos.



