import io
import sys
from PIL import Image
from google.cloud import storage

def convert_to_black_and_white(image):
  image_rgb = Image.open(io.BytesIO(image))
  image_gray = image_rgb.convert("L")
  return image_gray

def download_blob(bucket_name, source_folder, blob_name):
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_folder + blob_name)
    blob_bytes = blob.download_as_bytes()
    
    print("-> Downloaded storage object {} from bucket {}".format(blob_name, bucket_name))
    return blob_bytes

def upload_blob(bucket_name, source_file_name, destination_blob_name):
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    generation_match_precondition = 0
    blob.upload_from_filename(source_file_name, if_generation_match = generation_match_precondition)

    print("\n-> Updaload storage object {} to bucket {} to {}".format(blob.name, bucket_name, destination_blob_name))

if __name__ == "__main__":
    blob_bytes = download_blob(
        bucket_name = "cloud-misw-2",
        source_folder = "upload/",
        blob_name = "foto.png",
    )

    image_gray = convert_to_black_and_white(blob_bytes)
    image_gray.save("carpeta/temp.png")

    upload_blob(
        bucket_name = "cloud-misw-2",
        source_file_name = "carpeta/temp.png",
        destination_blob_name = "processed/foto-bn-1.png",
    )