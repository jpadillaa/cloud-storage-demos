from datetime import datetime, timedelta
from google.cloud import storage
from google.oauth2 import service_account

def generate_signed_url(bucket_name, object_name, expiration=3600, credentials_file_path='misw-soluciones-cloud.json'):
    """
    Genera una URL firmada para acceder al objeto en un bucket de Cloud Storage.

    Parámetros:
    - bucket_name: El nombre del bucket de Cloud Storage.
    - object_name: El nombre del objeto dentro del bucket.
    - expiration: Tiempo de expiración en segundos para la URL firmada (opcional, predeterminado: 3600 segundos).
    - credentials_file_path: Ruta al archivo de credenciales de la cuenta de servicio (opcional, predeterminado: 'misw-soluciones-cloud.json').
    
    Retorna:
    Una URL firmada válida para acceder al objeto durante el tiempo especificado.
    """
    # Crea una instancia del cliente de Cloud Storage con las credenciales de la cuenta de servicio
    credentials = service_account.Credentials.from_service_account_file(credentials_file_path)
    client = storage.Client(credentials=credentials)

    # Obtiene el bucket y el objeto
    bucket = client.bucket(bucket_name)
    blob = bucket.blob(object_name)

    # Genera la URL firmada con la expiración proporcionada en segundos
    signed_url = blob.generate_signed_url(expiration=expiration)

    return signed_url

if __name__ == "__main__":
    # Reemplaza 'nombre-del-bucket' y 'nombre-del-objeto' con tus propios valores
    bucket_name = 'cloud-misw-2'
    object_name = 'upload/foto.png'
    current_time = datetime.now()

    # Genera la URL firmada con una expiración de 1 hora (3600 segundos)
    signed_url = generate_signed_url(bucket_name, object_name, expiration= current_time + timedelta(seconds=3600))
    print("URL firmada:", signed_url)
